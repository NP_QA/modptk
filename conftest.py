import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options


@pytest.fixture(scope="session")
def browser_firefox():
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    driver = webdriver.Remote(command_executor="http://selenium__standalone-firefox:4444/wd/hub",
                              desired_capabilities={'browserName': 'firefox'})
    yield driver
    driver.quit()
