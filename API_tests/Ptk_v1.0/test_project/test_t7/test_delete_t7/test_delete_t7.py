import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API.Удаление Т7.')
@allure.story('Удаление Т7.')
@allure.severity('Critical')
def test_delete_t7(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782/t7"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    files = [('file', ('J_3334025_f20.xlsx', open('C:/Users/pupme/Downloads/J_3334025_f20.xlsx', 'rb'),
                       'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'))]
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("DELETE", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,files=files,
                                verify=False)
    print(response.text)
    with allure.step("Запрос на удаление Т7 отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на удаление Т7 отправлен, десериализируем ответ из json в словарь."):
        assert response.status_code == 200
    with allure.step(f"Посмотрим что получили "
                     f"{response.status_code}"):
        pass
    response = response.json()
    assert response["status"] == "success"
