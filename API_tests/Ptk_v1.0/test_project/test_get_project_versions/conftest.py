import json

import allure
import pytest
import requests


@pytest.fixture()
@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Авторизация в систему.')
@allure.story('Авторизация в систему.')
@allure.severity('Critical')
def fixture_give_cookies():
    auth_url = "https://dev.modptk.unc-esk.ru/auth/v1/auth/login"
    auth_payload = {}
    auth_headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Basic dXNlcjFAdW5jLnJ1OnVzZXIxQHVuYy5ydQ==',
        'Cookie': 'device_token=6505DATq2QeL4cXs10G8QJBjQc9NIWO6tlU6p76bQLelLwBYoJlpZUFUp460SGD1F',
        'Connection': 'keep-alive'
    }
    login_request = requests.post(url=auth_url, headers=auth_headers, data=auth_payload, verify=False)
    return login_request


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение токена доступа.')
@allure.story('Получение токена доступа.')
@allure.severity('Critical')
def get_access_token(fixture_give_access_token):
    url = "https://dev.modptk.unc-esk.ru/auth/v1/authorize"
    payload = json.dumps({
        "response_type": "token",
        "client_id": "main",
        "with": "user"
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dXNlcjFAdW5jLnJ1OnVzZXIxQHVuYy5ydQ=='
    }
    response = requests.post(url=url, headers=headers, data=payload, cookies=fixture_give_access_token.cookies,
                             verify=False)
    assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    response = response.json()
    assert response["status"] == "success"
    assert response["access_token"] != 0
    get_bearer_token = response["access_token"]
    return get_bearer_token
