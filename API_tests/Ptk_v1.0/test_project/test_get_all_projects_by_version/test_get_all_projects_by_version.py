import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение списка проектов в зависимости от версии.')
@allure.story('Получение списка проектов в зависимости от версии.')
@allure.severity('Critical')
def test_get_all_projects_by_version(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects?limit=10&offset=5"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    print(response.text)
    with allure.step("Запрос на получение списка проектов в зависимости от версии, отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение списка проектов в зависимости от версии, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["projects"] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
