import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Частичное редактирование проекта.')
@allure.story('Частичное редактирование проекта.')
@allure.severity('Critical')
def test_patch_project(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "name": "Реконструкциыя ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО \"МРСК Урала\" - \"Пермэнерго\")",
        "code": "H_3330677",
        "yearBegin": 2018,
        "yearEndOffer": 2022,
        "offerRationale": "Полная стоимость не изменилась. Уточнение графика производства работ.Изменение срока окончания реализации по причине переноса оплаты гарантийных удержаний на 2021 год.",
        "years": {
            "2021": "333.5"
        },
        "nds": 0.2
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    print(response.text)
    print(response.status_code)
    print(response.cookies.get_dict())
    with allure.step("Запрос на редактирование проекта отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на редактирование проекта отправлен, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["project"] != []
        assert response["project"][
                   "name"] == 'Реконструкциыя ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО "МРСК Урала" - "Пермэнерго")'
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass

    payload_undo = json.dumps({
        "name": "Реконструкция ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО \"МРСК Урала\" - \"Пермэнерго\")",
        "code": "H_3330677",
        "yearBegin": 2018,
        "yearEndOffer": 2022,
        "offerRationale": "Полная стоимость не изменилась. Уточнение графика производства работ.Изменение срока окончания реализации по причине переноса оплаты гарантийных удержаний на 2021 год.",
        "years": {
            "2021": "333.5"
        },
        "nds": 0.2
    })
    headers_undo = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response_undo = requests.request("PATCH", url, headers=headers_undo, data=payload_undo,
                                     cookies=fixture_give_cookies.cookies,
                                     verify=False)
    print(response_undo.text)
    print(response_undo.status_code)
    print(response_undo.cookies.get_dict())
    with allure.step("Запрос на возвращение проекта к исходному состоянию отправлен, посмотрим код ответа"):
        assert response_undo.status_code == 200, f"Неверный код ответа, получен {response_undo.status_code}"
    with allure.step("Запрос на возвращение проекта к исходному состоянию отправлен, десериализируем ответ из json в "
                     "словарь."):
        response_undo = response_undo.json()
        assert response_undo["status"] == "success"
        assert response_undo["project"] != []
        assert response_undo["project"][
                   "name"] == 'Реконструкция ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО "МРСК Урала" - "Пермэнерго")'
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass