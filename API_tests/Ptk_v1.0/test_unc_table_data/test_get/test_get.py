import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение информации о таблице УНЦ.')
@allure.story('Получение информации о таблице УНЦ.')
@allure.severity('Critical')
def test_get_unc_table_data(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/unc-table-datas/67"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на информации о таблицe УНЦ отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    response = response.json()
    with allure.step("Запрос на информации о таблицe УНЦ отправлен, десериализируем ответ из json в словарь."):
        pass
    assert response["status"] == "success"
    assert response["uncTableData"] != []
    assert response["uncTableData"]["uncTableId"] == 67
    assert response["uncTableData"]["header"] != []
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass

