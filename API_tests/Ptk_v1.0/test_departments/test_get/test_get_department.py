import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение филиала.')
@allure.story('Получение филиала.')
@allure.severity('Critical')
def test_get_department(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/departments/1"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на получение филиала отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение филиала отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
    assert response["status"] == "success"
    find_department = response["department"]["name"]
    assert response["department"] != []
    assert response["department"]["name"] == "МЭС Западной Сибири"
    with allure.step(
            "Проверяем найденный филиал" + f"{find_department}"):
        pass
