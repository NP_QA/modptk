import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение версии по названию и дню.')
@allure.story('Получение версии по названию и дню.')
@allure.severity('Critical')
def test_get_all_by_name_and_day(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/versions?name=K.2021.04&day=1622030266"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на получение версии по названию и дню отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    response = response.json()
    with allure.step("Запрос на получение списка версий отправлен, десериализируем ответ из json в словарь."):
        pass
    assert response["status"] == "success"
    print(response)
    assert response["versions"] != []
    assert response["versions"][0]["id"] != []
    assert response["versions"][0]["name"] == "K.2021.04"
    assert response["versions"][0]["isAskid"] == 1
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass

