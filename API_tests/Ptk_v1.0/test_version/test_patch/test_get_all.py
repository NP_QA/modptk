import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Частичное изменение версии: название.')
@allure.story('Частичное изменение версии: название.')
@allure.severity('Critical')
def test_patch_version(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/versions/2"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "name": "Переименовать"
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на частичное изменение версии: название отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    response = response.json()
    with allure.step("Запрос на частичное изменение версии: название отправлен, десериализируем ответ из json в словарь."):
        pass
    assert response["status"] == "success"
    assert response["version"] != []
    assert response["version"]["id"] != []
    assert response["version"]["name"] == "Переименовать"
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass

