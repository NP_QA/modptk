import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение таблицы УНЦ.')
@allure.story('Получение таблицы УНЦ.')
@allure.severity('Critical')
def test_get(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/unc-tables/67"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    print(response.text)
    print(response.cookies.get_dict())
    with allure.step("Запрос на получение таблицы УНЦ отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение таблицы УНЦ отправлен, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["uncTable"] != []
        assert response["uncTable"]['id'] == 67
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
