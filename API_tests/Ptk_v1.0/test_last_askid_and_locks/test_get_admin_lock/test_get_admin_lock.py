import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение региона.')
@allure.story('Получение региона.')
@allure.severity('Critical')
def test_get_admin_lock(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/system/locks/admin"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение региона отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение архива выгрузки Ф20 отправлен, десериализируем ответ из json в словарь."):
        pass
    response = response.json()
    assert response["status"] == "success"
    assert response["lock"] != []
    with allure.step(
            "Проверяем название найденного региона" + f"{response}"):
        pass
    url = "https://dev.modptk.unc-esk.ru/api/v1/system/locks/admin"
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("DELETE", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение региона отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение архива выгрузки Ф20 отправлен, десериализируем ответ из json в словарь."):
        pass
    response = response.json()
    assert response["status"] == "success"
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass