import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Частичное изменение программы.')
@allure.story('Частичное изменение программы.')
@allure.severity('Critical')
def test_patch_program(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/program"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "name": "Тест1овое название",
        "yearBegin": 2010,
        "yearEnd": 2025,
        "yearDisclosure": 2020,
        "deflators": {
            "2021": "33",
            "2022": 0.45
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на частичное изменение программы отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на частичное изменение программы отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["program"] != []
        assert response["program"]['id'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['program']}"):
        pass
