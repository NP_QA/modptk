import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Создание ячейки УНЦ в личном проекте')
@allure.story('Создание ячейки УНЦ в личном проекте.')
@allure.severity('Critical')
def test_post_create_unc_personal_project(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/personal-projects/16/personal-unc-estimation/personal-uncs?with=uncCell{@full},uncTable"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "regionId": 43,
        "count": 4,
        "uncCellId": 609,
        "tableNum": 2,
        "voltage": 110,
        "comment": None
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("POST", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на создание ячейки УНЦ в личном проекте отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на оздание ячейки УНЦ в личном проекте отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
    assert response["status"] == "success"
    assert response["personalUnc"] != []
    assert response["personalUnc"]["personalUncEstimationId"] == 12
    assert response["personalUnc"]["uncCell"] != []
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass