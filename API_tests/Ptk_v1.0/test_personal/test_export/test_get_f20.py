import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение выгрузки Ф20 из личного проекта.')
@allure.story('Получение выгрузки Ф20 из проекта.')
@allure.severity('Critical')
def test_get_f20(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/personal-projects/16/f20"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение выгрузки Ф20 из личного проекта отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение выгрузки Ф20 из личного проекта отправлен, десериализируем ответ из json в словарь."):
        pass
    get_attachment = response.headers['Content-Disposition']
    get_file_extension = (get_attachment[-5:])
    assert get_file_extension == '.xlsx'
    with allure.step(
            "Убедились в том, что к ответу на запрос прилагается файл расширения .xlsx =" + ' ' f"{get_attachment}"):
        pass