import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение личных расценок УНЦ')
@allure.story('Получение личных расценок УНЦ')
@allure.severity('Critical')
def test_get_project_with_unc_estimation(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/personal-projects/16?with=personalUncEstimation,user"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload={}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на получение личных расценок УНЦ отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение личных расценок УНЦ отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
    assert response["status"] == "success"
    assert response["personalProject"] != []
    assert response["personalProject"]["id"] != []
    assert response["personalProject"]["personalUncEstimation"] != []
    with allure.step(
            "Проверяем что получили" + f"{response}"):
        pass
