import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Частичное изменение личного проекта')
@allure.story('Частичное изменение личного проекта.')
@allure.severity('Critical')
def test_patch_personal_project(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/personal-projects"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "name": "Реконструкция ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО \"МРСК Урала\" - \"Пермэнерго\")",
        "departmentId": "8",
        "code": "H_3330577",
        "yearBegin": 2019,
        "yearEndOffer": 2022,
        "offerRationale": "Полная стоимость не изменилась. Уточнение графика производства работ.Изменение срока окончания реализации по причине переноса оплаты гарантийных удержаний на 2021 год.",
        "years": {
            "2021": "333.5"
        },
        "regionIds": [
            "43"
        ],
        "nds": 0.2,
        "deflators": {
            "2020": 104.5
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("POST", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на создание личного проекта отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на создание личного проекта отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
    assert response["status"] == "success"
    assert response["personalProject"] != []
    assert response["personalProject"]["id"] != []
    get_personal_project_id = response["personalProject"]["id"]
    url_patch = "https://dev.modptk.unc-esk.ru/api/v1/personal-projects"+"/"+f"{get_personal_project_id}"
    payload_patch = json.dumps({
        "name": "Рfsеконструкция ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО \"МРСК Урала\" - \"Пермэнерго\")",
        "code": "H_3330577",
        "yearBegin": 2018,
        "yearEndOffer": 2022,
        "offerRationale": "Test",
        "c15": 33,
        "deflators": {
            "2021": "104"
        },
        "years": {
            "2021": "333.5"
        },
        "regionIds": [
            "43"
        ],
        "nds": 0.2
    })
    response_patch = requests.request("PATCH", url_patch, headers=headers, data=payload_patch,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на изменение личного проекта отправлен, код ответа =" + ' ' f"{response_patch.status_code}"):
        assert response_patch.status_code == 200, f"Неверный код ответа, получен {response_patch.status_code}"
    with allure.step("Запрос на изменение личного проекта отправлен, десериализируем ответ из json в словарь."):
        response_patch = response_patch.json()
    assert response_patch["status"] == "success"
    assert response_patch["personalProject"] != []
    assert response_patch["personalProject"]["id"] != []
    assert response_patch["personalProject"]["id"] == get_personal_project_id
    assert response_patch["personalProject"]["name"] == "Рfsеконструкция ПС 220 кВ Титан. Установка АОПО (ВЛ 110 кВ Соликамск I,II цепь с отпайками). Реконструкция ПС 220 кВ Бумажная. Установка АОПО (АТ 1,2). Организация каналов связи УПАСК с ПС 220 кВ Титан, ПС 220 кВ Бумажная, ПС 500 кВ Калино на ПС 110 кВ Южный Рудник (для ТП объектов электросетевого хозяйства Филиала ОАО \"МРСК Урала\" - \"Пермэнерго\")"