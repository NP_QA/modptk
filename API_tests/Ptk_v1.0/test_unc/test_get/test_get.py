import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение информации по УНЦ.')
@allure.story('Получение информации по УНЦ.')
@allure.severity('Critical')
def test_get_unc(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782/uncs"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "regionId": 76,
        "count": 4,
        "uncCellId": 608,
        "tableNum": 2,
        "voltage": 110,
        "comment": None
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("POST", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на создание УНЦ отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на создание УНЦ отправлен отправлен, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["unc"]['id'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
    get_unc_id = response["unc"]['id']
    url = "https://dev.modptk.unc-esk.ru/api/v1/uncs/" + f"{str(get_unc_id)}"
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение информации по УНЦ отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение информации по УНЦ отправлен, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        print(response)
        assert response["unc"] != []
        assert response["unc"]["id"] == get_unc_id
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
