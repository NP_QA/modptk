import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Изменение списка УНЦ идентификаторов, версии.')
@allure.story('Изменение списка УНЦ идентификаторов, версии.')
@allure.severity('Critical')
def test_patch_current_version(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782/uncs"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "source": {
            "projectId": 2887
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на изменение списка УНЦ идентификаторов, версии, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step(
            "Запрос на изменение списка УНЦ идентификаторов, версии, десериализируем ответ из json "
            "в "
            "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["runUid"] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
