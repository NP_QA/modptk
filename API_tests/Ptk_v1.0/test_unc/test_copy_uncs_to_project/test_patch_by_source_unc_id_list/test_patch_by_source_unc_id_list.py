import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Изменение УНЦ списка УНЦ идентификаторов.')
@allure.story('Изменение УНЦ списка УНЦ идентификаторов.')
@allure.severity('Critical')
def test_patch_by_source_unc_id_list(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782/uncs"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "source": {
            "uncIds": [
                20501,
                20502,
                20503
            ]
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на изменение УНЦ списка УНЦ идентификаторов отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step(
            "Запрос на изменение УНЦ списка УНЦ идентификаторов отправлен, десериализируем ответ из json "
            "в "
            "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["runUid"] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
