import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение полной информации УНЦ по проекту и версии.')
@allure.story('Получение полной информации УНЦ по проекту и версии.')
@allure.severity('Critical')
def test_get_by_version_and_project_with_full_data(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/projects/2782/uncs?with=uncCell{@full},uncTable&versionId=1"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение полной информации УНЦ по проекту и версии отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step(
            "Запрос на получение полной информации УНЦ по проекту и версии отправлен, десериализируем ответ из json в "
            "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["uncs"] != []
        assert response["uncs"][1]["actualVersionId"] == 1

    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
