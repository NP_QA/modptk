import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Обновление записи УНЦ.')
@allure.story('Обновление записи УНЦ.')
@allure.severity('Critical')
def test_patch_update(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/uncs/37501"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "regionId": 76,
        "count": 7,
        "uncCellId": 609,
        "tableNum": 2,
        "voltage": 110,
        "comment": None
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("PATCH", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на обновление записи УНЦ отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step(
            "Запрос на обновление записи УНЦ отправлен, десериализируем ответ из json в "
            "словарь."):
        response = response.json()
        print(response)
        assert response["status"] == "success"
        assert response["unc"]['id'] != []
        assert response["unc"]['id'] == 37501
        assert response["unc"]['regionId'] == 76
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
