import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение списка проверок проекта.')
@allure.story('Получение списка проверок проекта.')
@allure.severity('Critical')
def test_get_all_sections(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/api/v1/rule-info/sections"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload={}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}"
    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)
    with allure.step("Запрос на получение списка проверок проекта отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение списка проверок проекта отправлен, десериализируем ответ из json в "
                     "словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["ruleSections"] != []
        assert response["ruleSections"][0]["id"] == 5
        assert response["ruleSections"][1]["id"] == 4
        assert response["ruleSections"][2]["id"] == 3
        assert response["ruleSections"][3]["id"] == 6
        assert response["ruleSections"][4]["id"] == 2
        assert response["ruleSections"][5]["id"] == 1
        assert response["runUid"] != []

    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
