import json

import allure
import pytest
import requests


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение токена доступа.')
@allure.story('Получение токена доступа.')
@allure.severity('Critical')
def test_get_access_token(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/auth/v1/authorize"
    payload = json.dumps({
        "response_type": "token",
        "client_id": "main",
        "with": "user"
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dXNlcjFAdW5jLnJ1OnVzZXIxQHVuYy5ydQ=='
    }
    response = requests.post(url=url, headers=headers, data=payload, cookies=fixture_give_cookies.cookies,
                             verify=False)
    with allure.step("Запрос на получение токена доступа отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение токена доступа отправлен, десериализируем ответ из json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["access_token"] != 0
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
    get_bearer_token = response["access_token"]
    return get_bearer_token
