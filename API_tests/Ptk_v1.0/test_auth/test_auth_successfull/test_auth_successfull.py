import allure
import pytest
import requests


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Авторизация в систему.')
@allure.story('Авторизация в систему.')
@allure.severity('Critical')
def test_auth_successfull():
    auth_url = "https://dev.modptk.unc-esk.ru/auth/v1/auth/login"
    auth_payload = {}
    auth_headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Basic dXNlcjFAdW5jLnJ1OnVzZXIxQHVuYy5ydQ==',
        'Cookie': 'device_token=6505DATq2QeL4cXs10G8QJBjQc9NIWO6tlU6p76bQLelLwBYoJlpZUFUp460SGD1F',
        'Connection': 'keep-alive'
    }

    login_request = requests.post(url=auth_url, headers=auth_headers, data=auth_payload, verify=False)
    print(login_request.text)
    with allure.step("Запрос на авторизацию отправлен, посмотрим код ответа"):
        assert login_request.status_code == 200, f"Неверный код ответа, получен {login_request.status_code}"
    with allure.step("Запрос на авторизацию отправлен. Десериализируем ответ из json в словарь."):
        response = login_request.json()
        assert response["status"] == "success"
    with allure.step(f"Посмотрим что получили "
                     f"{response}"):
        pass
