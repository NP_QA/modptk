import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение актуального расчета проекта.')
@allure.story('Получение актуального расчета проекта.')
@allure.severity('Critical')
def test_get_usp(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/usp/v1/projects/2823"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на получение актуального расчета проекта отправлен отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение получение актуального расчета проекта отправлен, десериализируем ответ из "
                     "json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["result"] != []
        assert response["result"]['id'] != []
        assert response["result"]['actualEstimation'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['result']}"):
        pass
