import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Обновление расчета по ТКП.')
@allure.story('Обновление расчета по ТКП.')
@allure.severity('Critical')
def test_put_tkp(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/usp/v1/projects/3033/tkp"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "common": {
            "year": 2021,
            "quarter": 3,
            "reductionCoefficientManual": 0.87
        },
        "tkp": {
            "meaning": 1,
            "month": 12,
            "costItem": 1,
            "countByTkp": 1,
            "countByProject": 1,
            "tkp": [
                {
                    "number": "in in Duis pariatur nulla",
                    "price": 100500.99
                },
                {
                    "number": "culpa Excepteur mollit",
                    "price": 100500.99
                }
            ]
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("PUT", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на обновление расчета по ТКП отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на обновление расчета по ТКП отправлен, десериализируем ответ из "
                     "json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["result"] != []
        assert response["result"]['type'] == "tkp"
        assert response["result"]['uncForecastPrice'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['result']}"):
        pass
