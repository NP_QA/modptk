import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Обновление расчета по объекту-аналогу.')
@allure.story('Обновление расчета по объекту-аналогу.')
@allure.severity('Critical')
def test_put_oa(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/usp/v1/projects/2823/oa"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "common": {
            "year": 2020,
            "quarter": 3,
            "reductionCoefficientManual": 0.87
        },
        "oa": {
            "title": "elit ut non minim",
            "details": "elit irure nulla in commodo",
            "regionId": 1,
            "basicPir": 100500.99,
            "basicSmr": 100500.99,
            "basicEquipment": 100500.99,
            "basicOther": 100500.99,
            "basicCount": 1000,
            "projectPir": 100500.99,
            "projectSmr": 100500.99,
            "projectEquipment": 100500.99,
            "projectOther": 100500.99,
            "projectCount": 1000
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("PUT", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на обновление расчета по объекту-аналогу отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на обновление расчета по объекту-аналогу отправлен, десериализируем ответ из "
                     "json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["result"] != []
        assert response["result"]['type'] == "oa"
        assert response["result"]['uncForecastPrice'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['result']}"):
        pass
