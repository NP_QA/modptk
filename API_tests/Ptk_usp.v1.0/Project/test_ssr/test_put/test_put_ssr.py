import json

import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Обновление расчета по ССР.')
@allure.story('Обновление расчета по ССР.')
@allure.severity('Critical')
def test_put_ssr(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/usp/v1/projects/3608/ssr"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = json.dumps({
        "common": {
            "year": 2020,
            "quarter": 3,
            "reductionCoefficientManual": 0.87
        },
        "ssr": {
            "details": "consequat",
            "pirPrice": 100500.99,
            "equipmentPrice": 100500.99,
            "otherPrice": 100500.99,
            "smr": [
                {
                    "regionId": 1,
                    "price": 100500.99,
                    "coefficient": 0.7
                },
                {
                    "regionId": 1,
                    "price": 100500.99,
                    "coefficient": 0.7
                }
            ]
        }
    })
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("PUT", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на обновление расчета по ССР отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на обновление расчета по ССР отправлен, десериализируем ответ из "
                     "json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["result"] != []
        assert response["result"]['type'] == "ssr"
        assert response["result"]['uncForecastPrice'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['result']}"):
        pass
