import allure
import pytest
import requests

from conftest import get_access_token


@pytest.mark.smoke_api_test_modptk
@allure.feature('Smoke тестирование API. Получение списка секций.')
@allure.story('Получение списка секций.')
@allure.severity('Critical')
def test_get_usp(fixture_give_cookies):
    url = "https://dev.modptk.unc-esk.ru/usp/v1/sections"
    get_bearer_token = get_access_token(fixture_give_cookies)
    payload = {}
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Authorization': 'Bearer' + ' ' f"{str(get_bearer_token)}",

    }
    response = requests.request("GET", url, headers=headers, data=payload,
                                cookies=fixture_give_cookies.cookies,
                                verify=False)

    with allure.step("Запрос на получение списка секций отправлен, код ответа =" + ' ' f"{response.status_code}"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
    with allure.step("Запрос на получение списка секций отправлен, десериализируем ответ из "
                     "json в словарь."):
        response = response.json()
        assert response["status"] == "success"
        assert response["sections"] != []
        assert response["sections"][0]['id'] != []
        assert response["sections"][0]['name'] != []
        assert response["sections"][0]['tablesIds'] != []
    with allure.step(f"Посмотрим что получили "
                     f"{response['status']}, {response['sections']}"):
        pass
